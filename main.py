from aiohttp import web
app = web.Application()
routes = web.RouteTableDef()

@routes.get("/")
def index(request: web.Request):
    return web.Response(text= f"Hello, {request.query.get('name', 'Anonymous')}")

if __name__ == "__main__":
    app.add_routes(routes)
    web.run_app(app, host= "0.0.0.0", port= 0000)
